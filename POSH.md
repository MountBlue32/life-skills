# Prevention of Sexual Harassment

## Kinds of behaviour causing sexual harassment:

- Verbal
  - Comments on clothing
  - Comments on body
  - Sexual or gender based remarks
  - Requesting sexual favours
  - Repeatedly asking a person out
  - Sexual innuendos (when something one says is sounds innocent but means graphically sexual)
  - Threats
  - Rumour-mongering
  - Using foul and obscene language
- Visuals of sexual nature
  - Posters
  - Drawings
  - Screensavers
  - Cartoons
  - Emails or Texts
- Physical

  - Sexual assault
  - Impeding or blocking movement
  - Inappropriate touching
  - Sexual gesturing
  - Leering (looking at someone in an unpleasant or offensive way) or staring

  - Quid Pro Quo (asking for favours in return for a promotion or threat of a demotion)
  - Hostile work environment

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- I will ask the person involved to stop.
- If he/she doesn't stop, then I would bring the incident to my supervisor's notice.
- If a supervisor is involved in the incident, I would report to his/her supervisor.
- Be prepared to resort to acts of self-defence.
