# Listening and Active Communication

## What are the steps/strategies to do Active Listening?

- Listen to absorb the knowledge, not to give a reply.
- Lending a sympathetic ear can be much more impactful than telling someone what they should do.
- Take notes during important meetings, especially in a professional setting.
- Asking relevant questions that shows that you’ve been listening and clarify doubts regarding what has been said.
- Maintain good eye contact but do not stare.
- Make sure to ask for additional information if required, or provide an interesting opinion, which will show your engagement.

## According to Fisher's model, what are the key points of Reflective Listening?

- Eliminate all distractions.
- Respond to the speaker frequently.
- Summarise the content of the speaker.
- Reflect the mood of the speaker with words and non-verbal queues.

## What are the obstacles in your listening process?

- Drifting off into a stream of personal thoughts when the person is continuously speaking.

## What can you do to improve your listening?

- Summarizing what the speaker says.
- Making notes.
- Improve my spoken English.

## When do you switch to Passive communication style in your day to day life?

- During conversations with figures of authority or power.

## When do you switch into Aggressive communication styles in your day to day life?

- During times of fight or flight, or moments of great excitement.
- During times of adversity.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- When I am with friends and loved ones, duing periods of relaxation.

## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

- Choosing the correct timing and the correct way to speak things (inculcating diplomacy, etc).
- Deploying a non- emotional, dispassionate and logical outlook towards all issues.
