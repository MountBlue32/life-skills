# Good Practices for Software Development

## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

- Sometimes inspite of all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page

- Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.

- Look at the way issues get reported in large open-source projects

- Join the meetings 5-10 mins early to get some time with your team members

- Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation

- Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I have to improve my React skills quickly, especially the class components.

- I have to regularly practice the older topics so as to not get out of touch with it.

- I have to improve on my programming best practices and work on open source.

- Be updated regularly on the internet related to the latest javascript and react features.
