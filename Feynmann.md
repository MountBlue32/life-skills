# Learning Process

## Question 1

> He who cannot explain a thing simply, has not understood them enough.

It is necessary to learn well enough that one can explain it as simply as possible to the level of a commoner. To have an indepth knowlegde of a topic, it is always good to self-explain or to explain the concept to someone else. Explaining the concept helps us know the topics we are clear enough and those that are unclear. Feynman once said not to fool oneself, as it is the easiest thing to do to oneself, while learning. To make learning a simple process, Feynman suggested four steps:

1- Write down the topic which we are learning on the top

2- Explain the concept in simple language - concepts with example

3- Identify the places/concepts where you are shaky

4- Identify the unfamiliar terms and break them down into simple words for understanding

This technique is helpful because it helps find one's strengths and also to find out the weak zones in your learning. Also it makes effective use of your time in the learning process. Note that making it simple doesn't mean keeping the terms alone simple, but being ready to find out why we need the concept, when and where to apply the things we have learnt.

## Question 2

We can implement this in our learning by

- Jotting down whatever topics we have covered
- Explaining self in easy terms
- Explaining the concept learnt to peers
- Discuss with others like a questionnaire session
- Note down new terms and try to mind-map them to easy stuff to remember

## Question 3

The TED Talker Barbara Oakley, went in search of finding out effective ways of learning and had taken references even from the lives of Dali and Edison on how they kept themselves focused with their works.

To understand how our mind works she researched neuroscience and cognitive psychology. She found out that our brain functions in 2 modes: Focus mode and Relaxed mode. In focused mode, the neural states are together focusing on the specific subject whereas in relaxing mode the neural states are diffused. However, while learning our mind does not stay only in any particular state but it goes back ND FORTH BETWEEN THE TWO STATES. Although this is normal, at times we tend to procrastinate things and this is because we feel a neural pain of how much effort that task needs to get it over with and eventually we end up doing what makes our brain feel light and happy which makes it get distracted from learning. Hence she suggested following the Pomodoro technique wherein we decide to have a mindset that we can learn for 25 minutes with full focus and take rest for the next 5 minutes. This 25 minutes of full focus is achieved by keeping ourselves away from any kind of distractions like mobile pings and this period is not to get all our work completed within 25 minutes but to do the learning mindfully. This way we are enhancing our ability for focused attention while learning.

Sometimes how much ever we do focus we tend to get distracted often and this is often with creative brains as they tend to think much more than what is being learnt. And some people consider themselves slow thinkers but they have a better grasp towards the concepts firmly than rushing over the concepts vaguely.

At times we feel that we have covered the concepts but while testing we get jitters. So to avoid such anxiety it is always better to test oneself as many times as possible. Try to work on the concept more and more until you are familiar with it. Because more often you keep coming across the concept, the more often you can recall it and more you become the master of the subject. This is because our brain knows the flow of how to understand the concept quicker. To conclude, the most powerful tool in learning is learning how to learn things.

## Question 4

Steps to improve your learning process

- Follow the Pomodro technique while learning
- Learning things with good clarity is much better than learning faster with less familiarity
- It is always good to test one's level of learning by checking with mini tests
- Try to recall of things before you start with a new concept and carry on thereafter with new concepts

## Question 5

Key takeaways from the video

- It takes around 20 hours to become reasonably good at a new skill
- The new skill cannot be learnt without any proper self-motivation or curiosity
- The more time you spend on the skill, the better you become
- The 20 hours of practice is possible if we can spend 45 minutes a day for a month without even skipping a couple of days
- So there are 4 essential things to consider before learning anything new:
  1.  Deconstruct the skill to decide what you want to do once you finished learning this skill
  2.  Learn enough to self-correct and be self-consistent with the progress
  3.  Remove barriers to practise - any kind of distractions and increase your willpower to remove the distractions
  4.  Ensure that you have spent at least 20 hours practise that skill to become pretty good at it

## Question 6

- Be clear about what to learn so that at end we can get what we expected to achieve by learning the skill
  -It is not necessary to learn everything and become an expert but being consistent in learning everyday helps in acquiring more knowledge
- Learn with multiple references and try to do self-correction
- Keep oneself away from distractions while learning
- Ensure that the amount of time spent to learn that skill is utilised thoroughly only for learning it in depth with utmost focus
