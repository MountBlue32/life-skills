## object-oriented programming (OOP)

## What is object-oriented programming?

* Object-oriented programming (OOP) is a computer programming model that organizes software design around data, or objects, rather than functions and logic. An object can be defined as a data field that has unique attributes and behavior.

* OOP focuses on the objects that developers want to manipulate rather than the logic required to manipulate them. This approach to programming is well-suited for programs that are large, complex and actively updated or maintained. This includes programs for manufacturing and design, as well as mobile applications; for example, OOP can be used for manufacturing system simulation software.

* The organization of an object-oriented program also makes the method beneficial to collaborative development, where projects are divided into groups. Additional benefits of OOP include code reusability, scalability and efficiency.

* The first step in OOP is to collect all of the objects a programmer wants to manipulate and identify how they relate to each other -- an exercise known as [data modeling](https://www.techtarget.com/searchdatamanagement/definition/data-modeling).

* Examples of an object can range from physical entities, such as a human being who is described by properties like name and address, to small computer programs, such as [widgets](https://www.techtarget.com/whatis/definition/widget).

* Once an object is known, it is labeled with a [class](https://www.techtarget.com/whatis/definition/class) of objects that defines the kind of data it contains and any logic sequences that can manipulate it. Each distinct logic sequence is known as a method. Objects can communicate with well-defined interfaces called messages.


![](https://cdn.ttgtmedia.com/rms/onlineimages/whatis-object_oriented_programming.png)

## What is the structure of object-oriented programming?

 The structure, or building blocks, of object-oriented programming include the following:

* **Classes** are user-defined data types that act as the blueprint for individual objects, attributes and methods.

* **Objects** are instances of a class created with specifically defined data. Objects can correspond to real-world objects or an abstract entity. When class is defined initially, the description is the only object that is defined.
* **Methods** are functions that are defined inside a class that describe the behaviors of an object. Each method contained in class definitions starts with a reference to an instance object. Additionally, the subroutines contained in an object are called instance methods. Programmers use methods for reusability or keeping functionality encapsulated inside one object at a time.

* **Attributes** are defined in the class template and represent the state of an object. Objects will have data stored in the attributes field. Class attributes belong to the class itself.


## The structure of OOP.

What are the main principles of OOP?

Object-oriented programming is based on the following principles:

* **Encapsulation**. This principle states that all important information is contained inside an object and only select information is exposed. The implementation and state of each object are privately held inside a defined class. Other objects do not have access to this class or the authority to make changes. They are only able to call a list of public functions or methods. This characteristic of data hiding provides greater program security and avoids unintended data corruption.

* **Abstraction**. Objects only reveal internal mechanisms that are relevant for the use of other objects, hiding any unnecessary implementation code. The derived class can have its functionality extended. This concept can help developers more easily make additional changes or additions over time.

* **Inheritance**. Classes can reuse code from other classes. Relationships and subclasses between objects can be assigned, enabling developers to reuse common logic while still maintaining a unique hierarchy. This property of OOP forces a more thorough data analysis, reduces development time and ensures a higher level of accuracy.

* **Polymorphism**. Objects are designed to share behaviors and they can take on more than one form. The program will determine which meaning or usage is necessary for each execution of that object from a parent class, reducing the need to duplicate code. A child class is then created, which extends the functionality of the parent class. Polymorphism allows different types of objects to pass through the same interface.


## What are examples of object-oriented programming languages?

While [Simula](https://www.techtarget.com/whatis/definition/Simula-simulation-language) is credited as being the first object-oriented programming language, many other programming languages are used with OOP today. But some programming languages pair with OOP better than others. For example, programming languages considered pure OOP languages treat everything as objects. Other programming languages are designed primarily for OOP, but with some procedural processes included.

For example, popular pure OOP languages include:

* [Ruby](https://www.techtarget.com/whatis/definition/Ruby)
* Scala
* JADE
* Emerald

Programming languages designed primarily for OOP include:

* Java
* Python
* C++

Other programming languages that pair with OOP include:

* Visual Basic .NET
* [PHP](https://www.techtarget.com/whatis/definition/Personal-Home-Page-PHP)
* JavaScript


## What are the benefits of OOP?

Benefits of OOP include:

* **Modularity**. Encapsulation enables objects to be self-contained, making troubleshooting and collaborative development easier.

* **Reusability**. Code can be reused through inheritance, meaning a team does not have to write the same code multiple times.

* **Productivity**. Programmers can construct new programs quicker through the use of multiple libraries and reusable code.

* **Easily upgradable and scalable**. Programmers can implement system functionalities independently.

* **Interface descriptions**. Descriptions of external systems are simple, due to message passing techniques that are used for objects communication.

* **Security**. Using encapsulation and abstraction, complex code is hidden, software maintenance is easier and internet protocols are protected.

* **Flexibility**. Polymorphism enables a single function to adapt to the class it is placed in. Different objects can also pass through the same interface.


## Criticism of OOP

The object-oriented programming model has been criticized by developers for multiple reasons. The largest concern is that OOP overemphasizes the data component of software development and does not focus enough on computation or algorithms. Additionally, OOP code may be more complicated to write and take longer to compile.

Alternative methods to OOP include:

* **Functional programming**. This includes languages such as Erlang and Scala, which are used for telecommunications and fault tolerant systems.

* **Structured or modular programming**. This includes languages such as PHP and C#.

* **Imperative programming**. This alternative to OOP focuses on function rather than models and includes C++ and Java.

* **Declarative programming**. This programming method involves statements on what the task or desired outcome is but not how to achieve it. Languages include Prolog and Lisp.

* **Logical programming**. This method, which is based mostly in formal logic and uses languages such as Prolog, contains a set of sentences that express facts or rules about a problem domain. It focuses on tasks that can benefit from rule-based logical queries.

Most advanced programming languages enable developers to combine models, because they can be used for different programming methods. For example, JavaScript can be used for OOP and functional programming.

Developers who are working with OOP and microservices can address common microservices issues by applying the principles of OOP.

# Classes and instances

When we model a problem in terms of objects in OOP, we create abstract definitions representing the types of objects we want to have in our system. For example, if we were modeling a school, we might want to have objects representing professors. Every professor has some properties in common: they all have a name and a subject that they teach. Additionally, every professor can do certain things: they can all grade a paper and they can introduce themselves to their students at the start of the year, for example.

So Professor could be a class in our system. The definition of the class lists the data and methods that every professor has.

In pseudocode, a Professor class could be written like this:

    class Professor
    properties
        name
        teaches
    methods
        grade(paper)
        introduceSelf()

This defines a Professor class with:

two data properties: name and teaches
two methods: grade() to grade a paper and introduceSelf() to introduce themselves.
On its own, a class doesn't do anything: it's a kind of template for creating concrete objects of that type. Each concrete professor we create is called an instance of the Professor class. The process of creating an instance is performed by a special function called a constructor. We pass values to the constructor for any internal state that we want to initialize in the new instance.

Generally, the constructor is written out as part of the class definition, and it usually has the same name as the class itself:

    class Professor
    properties
        name
        teaches
    constructor
        Professor(name, teaches)
    methods
        grade(paper)
        introduceSelf()


This constructor takes two parameters, so we can initialize the name and teaches properties when we create a new concrete professor.

Now that we have a constructor, we can create some professors. Programming languages often use the keyword new to signal that a constructor is being called.

    walsh = new Professor("Walsh", "Psychology");
    lillian = new Professor("Lillian", "Poetry");

    walsh.teaches; // 'Psychology'
    walsh.introduceSelf(); // 'My name is Professor Walsh and I will be your Psychology professor.'

    lillian.teaches; // 'Poetry'
    lillian.introduceSelf(); // 'My name is Professor Lillian and I will be your Poetry professor.'

This creates two objects, both instances of the Professor class.

# Inheritance

Suppose in our school we also want to represent students. Unlike professors, students can't grade papers, don't teach a particular subject, and belong to a particular year.

However, students do have a name and may also want to introduce themselves, so we might write out the definition of a student class like this:

    class Student
    properties
        name
        year
    constructor
        Student(name, year)
    methods
        introduceSelf()

It would be helpful if we could represent the fact that students and professors share some properties, or more accurately, the fact that on some level, they are the same kind of thing. Inheritance lets us do this.

We start by observing that students and professors are both people, and people have names and want to introduce themselves. We can model this by defining a new class Person, where we define all the common properties of people. Then, Professor and Student can both derive from Person, adding their extra properties:

    class Person
        properties
            name
        constructor
            Person(name)
        methods
            introduceSelf()

    class Professor : extends Person
        properties
            teaches
        constructor
            Professor(name, teaches)
        methods
            grade(paper)
            introduceSelf()

    class Student : extends Person
        properties
            year
        constructor
            Student(name, year)
        methods
            introduceSelf()

In this case, we would say that Person is the superclass or parent class of both Professor and Student. Conversely, Professor and Student are subclasses or child classes of Person.

You might notice that **introduceSelf()** is defined in all three classes. The reason for this is that while all people want to introduce themselves, the way they do so is different:

    walsh = new Professor("Walsh", "Psychology");
    walsh.introduceSelf(); // 'My name is Professor Walsh and I will be your Psychology professor.'

    summers = new Student("Summers", 1);
    summers.introduceSelf(); // 'My name is Summers and I'm in the first year.'

We might have a default implementation of introduceSelf() for people who aren't students or professors:

    pratt = new Person("Pratt");
    pratt.introduceSelf(); // 'My name is Pratt.'

This feature - when a method has the same name but a different implementation in different classes - is called **polymorphism**. When a method in a subclass replaces the superclass's implementation, we say that the subclass **overrides** the version in the superclass.

# Encapsulation

Objects provide an interface to other code that wants to use them but maintain their own internal state. The object's internal state is kept **private**, meaning that it can only be accessed by the object's own methods, not from other objects. Keeping an object's internal state private, and generally making a clear division between its public interface and its private internal state, is called **encapsulation**.

This is a useful feature because it enables the programmer to change the internal implementation of an object without having to find and update all the code that uses it: it creates a kind of firewall between this object and the rest of the system.

For example, suppose students are allowed to study archery if they are in the second year or above. We could implement this just by exposing the student's year property, and other code could examine that to decide whether the student can take the course:

    if (student.year > 1) {
    // allow the student into the class
    }

The problem is, if we decide to change the criteria for allowing students to study archery - for example by also requiring the parent or guardian to give their permission - we'd need to update every place in our system that performs this test. It would be better to have a **canStudyArchery**() method on **Student** objects, that implements the logic in one place:

    class Student : extends Person
        properties
        year
        constructor
            Student(name, year)
        methods
        introduceSelf()
        canStudyArchery() { return this.year > 1 }

    if (student.canStudyArchery()) {
    // allow the student into the class
    }

That way, if we want to change the rules about studying archery, we only have to update the **Student** class, and all the code using it will still work.

In many OOP languages, we can prevent other code from accessing an object's internal state by marking some properties as **private**. This will generate an error if code outside the object tries to access them:

    class Student : extends Person
        properties
        private year
        constructor
            Student(name, year)
        methods
        introduceSelf()
        canStudyArchery() { return this.year > 1 }

    student = new Student('Weber', 1)
    student.year // error: 'year' is a private property of Student

# OOP and JavaScript #

In this article, we've described some of the basic features of class-based object-oriented programming as implemented in languages like Java and C++.

In the two previous articles, we looked at a couple of core JavaScript features: [constructors](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Basics) and [prototypes](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_prototypes). These features certainly have some relation to some of the OOP concepts described above.

* **constructors** in JavaScript provide us with something like a class definition, enabling us to define the "shape" of an object, including any methods it contains, in a single place. But prototypes can be used here, too. For example, if a method is defined on a constructor's prototype property, then all objects created using that constructor get that method via their prototype, and we don't need to define it in the constructor.

* **the prototype chain** seems like a natural way to implement inheritance. For example, if we can have a Student object whose prototype is Person, then it can inherit name and override introduceSelf().

But it's worth understanding the differences between these features and the "classical" OOP concepts described above. We'll highlight a couple of them here.

First, in class-based OOP, classes and objects are two separate constructs, and objects are always created as instances of classes. Also, there is a distinction between the feature used to define a class (the class syntax itself) and the feature used to instantiate an object (a constructor). In JavaScript, we can and often do create objects without any separate class definition, either using a function or an object literal. This can make working with objects much more lightweight than it is in classical OOP.

Second, although a prototype chain looks like an inheritance hierarchy and behaves like it in some ways, it's different in others. When a subclass is instantiated, a single object is created which combines properties defined in the subclass with properties defined further up the hierarchy. With prototyping, each level of the hierarchy is represented by a separate object, and they are linked together via the __proto__ property. The prototype chain's behavior is less like inheritance and more like delegation. Delegation is a programming pattern where an object, when asked to perform a task, can perform the task itself or ask another object (its delegate) to perform the task on its behalf. In many ways, delegation is a more flexible way of combining objects than inheritance (for one thing, it's possible to change or completely replace the delegate at run time).

That said, constructors and prototypes can be used to implement class-based OOP patterns in JavaScript. But using them directly to implement features like inheritance is tricky, so JavaScript provides extra features, layered on top of the prototype model, that map more directly to the concepts of class-based OOP.


# Resources

- [What is OOPs](https://www.techtarget.com/searchapparchitecture/definition/object-oriented-programming-OOP)
- [YouTube video : Object-oriented Programming in 7 minutes | Mosh](https://www.youtube.com/watch?v=pTB0EiLXUC8)
- [YouTube video : Intro to Object Oriented Programming - Crash Course by FreeCodeCamp](https://www.youtube.com/watch?v=SiBw7os-_zI)
- [Wikipedia : Object-oriented programming](https://en.wikipedia.org/wiki/Object-oriented_programming)

