# Grit and Growth Mindset

## 1. Grit

### Q1. Paraphrase (summarize) the video in a few lines. Use your own words.

- Those who are tenacious can acheive more than those who are just talented.
- Studies show that gritty people endure despite facing trying circumstances.
- Keeping our promises can help us develop grit.
- Talent is inversely correlated with grit.
- Grit can only be developed by adopting an attitude of personal growth.

### Q2. What are your key takeaways from the video to take action on?

- Grit, not IQ, is what matters most in order to achieve in life.
- Grit is the desire to achieve a goal in spite of adverse circumstances.
- Avoid giving up when you fail. Learn and grow instead.
- Keep trying after failing.
- One's performance is improved by having a growth mentality.

## 2. Introduction to Growth Mindset

### Q3. Paraphrase (summarize) the video in a few lines in your own words.

Having a growth mindset can help you lead a successful life. To improve your life, you need to alter your perspective. To adopt a growth mindset, one must make an effort to learn, accept difficulties and give them your best, accept failures and turn them into successes, take feedback and work on it.

### Q4. What are your key takeaways from the video to take action on?

- Believe in yourself that you can figure out things and challenges.
- Do not quit.
- Say I believe I can take a break today and then bounce back.
- Believe in yourself that you can do anything challenge yourself.

## 3. Understanding Internal Locus of Control

### Q5. What is the Internal Locus of Control? What is the key point in the video?

- The internal locus of control is what drives you to do the task: it is the ability to ask yourself, "Why am I not doing this?, Why can't I do this?" and then you roll up your sleeves and do the work. These are the factors that prompted you to disregard excuses and begin looking for reasons to complete the task. It is the state in which you are self-motivated.

- This video highlighted many important points to remember, such as the importance of remaining positive, self-motivated, looking for reasons to complete the task, and never look for excuses. This video taught us to be doers rather than complainers, and that the only thing standing between us and success is an emotional and mental barrier.

## 4. How to build a Growth Mindset

### Q6. Paraphrase (summarize) the video in a few lines in your own words.

- A growth mentality is based on the notion that everything is possible.
- Do not assume that you cannot enhance your abilities. Instead, consider who you are now and what you can become.
- Believe that you can change; don't think that who you are today will be the same in the future.
- Create your own life's structure and schedule.
- Avoid discouragement when you fail and accept the challenges.

### Q7. What are your key takeaways from the video to take action on?

- Believe in yourself and your ability to achieve better results through hard work. Question yourself about what you want to become and what you can do to get there can change your mindset.
- Focusing on work can improve our results.
- Don't get discouraged when mistakes happen.
- Accept the challenges, even though they are tough.

## 5. Mindset - A MountBlue Warrior Reference Manual

### Q8. What are one or more points that you want to take action on from the manual?

- I will stick to a problem till I complete it.
- I will understand each concept properly.
- I will have confidence in myself and stand by it.
