# Focus Management

## What is Deep Work?

- Working without distractions and improvement of productivity.

## Paraphrase all the ideas in the above videos and this one in detail.

- The first video describes deep work, and the duration of work without taking breaks, the video describes that the minimum duration of deep work should be 1 hour.
- The second video talks about deadlines and their effectiveness. They work because of several reasons :-
  - Deadlines serve as motivation to work, as one tries to finish the work before the deadline.
  - Deadlines help regulate breaks as one can plan how long they need to work before taking a break.
- Third video talks about the fact that hard work leads to the production of myelin, which increases brain function.
- The video also talks about some strategies to implement deep work which include:
  - Schedule distractions or breaks.
  - Make deep work a habit or like a sinusoidal function of time.
  - Get adequate sleep.

## How can you implement the principles in your day to day life?

- Some steps that implement deep work in our day-to-day life are :-
  - At least practice deep work for 1 hour a day.
  - Schedule breaks.
  - Practice deep work regularly and make it a habit.
  - Get adequate sleep.

## Your key takeaways from the video.

- Some key points of the video :
  - Social Media wastes too much time and is unhealthy for mind and body.
  - Social Media keeps distracting as it is designed to keep your attention for as long as possible.
  - Use of social media is bad for one's mental health as it leads to comparison of lifestyles which can depress a person.
